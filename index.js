import './db';
import dotenv from 'dotenv';
import express from 'express';
import moviesRouter from './api/movies';
import genresRouter from './api/genres';
import upcomingRouter from './api/moviesUpcoming';
import nowPlayingRouter from './api/moviesNowplaying';
import bodyParser from 'body-parser';
import usersRouter from './api/users';
import session from 'express-session';
import loglevel from 'loglevel';
// replace existing import with passport strategy​

import passport from './authenticate';
import {loadUsers, loadMovies, loadGenres, loadNowPlayingMovies, loadUpcomingMovies} from './seedData';


dotenv.config();

if (process.env.NODE_ENV === 'test') {
  loglevel.setLevel('warn');
} else {
  loglevel.setLevel('info');
}

const errHandler = (err, req, res, next) => {
  /* if the error in development then send stack trace to display whole error,
  if it's in production then just send error message  */
  if(process.env.NODE_ENV === 'production') {
    return res.status(500).send(`Something went wrong!`);
  }
   res.status(500).send(`Hey!! You caught the error 👍👍, ${err.stack} `);
};

const app = express();

const port = process.env.PORT;

//configure body-parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded())

if (process.env.SEED_DB) {
  loadUsers();
  loadMovies();
  loadGenres();
  loadNowPlayingMovies();
  loadUpcomingMovies();
}

// initialise passport​
app.use(passport.initialize())

//session middleware
app.use(session({
  secret: 'ilikecake',
  resave: true,
  saveUninitialized: true
}));



app.use(express.static('public'));
// Add passport.authenticate(..)  to middleware stack for protected routes​
app.use('/api/movies', moviesRouter);
app.use('/api/genres', genresRouter);
app.use('/api/upcoming', upcomingRouter);
app.use('/api/nowplaying', nowPlayingRouter);

//Users router
app.use('/api/users', usersRouter);

app.use(errHandler);

let server = app.listen(port, () => {
  loglevel.info(`Server running at ${port}`);
});

module.exports = server;