# Assignment 2 - (Agile) Movies API app. 

Name: Ciaran Whitty 20085909

## Features (Testing).

### Movies:
 + Feature 1 - GET /movies, (should return 20 movies and a status 200).
 + Feature 2 - GET /movies/:id, when the id is valid, (when the id is valid).
 + Feature 3 - GET /movies/:id, when the id is invalid, (should return the NOT found message).

### Users:
 + Feature 1 - GET /users, (should return the 2 users and a status 200).
 + Feature 2 - POST /, (should return a 200 status and the confirmation message).

## Setup requirements.

### Variables Need for setup:

The Variables below need to be added with GitLab and Heroku,

+ HEROKU_API_KEY (Heroku Account API)
+ HEROKU_APP_NAME_STAGING (Heroku App Name)
+ mongoDB (cloud MongoDB)
+ TMDB_KEY 
+ SECRET (Auth SECRET)

## Routing (Heroku).

+ /api/movies
+ /api/users
+ /api/upcoming
+ /api/nowplaying

## Links.

### Heroku.

+ https://a2-movies-api-staging-20085909.herokuapp.com/

### GitLab.

+ https://gitlab.com/CiaranWhitty/moviesapp-api-ci-assignment

---------------------------------
