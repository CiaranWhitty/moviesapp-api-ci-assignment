import express from 'express';
import MoviesNowplaying from './movieNowplayingModel'

const router = express.Router();

router.get('/', (req, res, next) => {
  MoviesNowplaying.find().then(movies => res.status(200).send(movies)).catch(next);
});

router.get('/:id', (req, res, next) => {
  const id = parseInt(req.params.id);
  MoviesNowplaying.findByMovieDBId(id).then(movie => res.status(200).send(movie)).catch(next);
});


export default router;